<?php

if( !current_user_can( 'activate_plugins' ) ) return;

// Post author id
if( current_user_can( 'activate_plugins' ) )
{
	$author_id = get_current_user_id();
}

// global declaration
$shortcode = RB_SHORTCODE;
$shortcode_two = RB_SHORTCODE_IFRAME;
$page_slug = 'room-booking';
$page_slug_two = 'room-booking-step-2';
$post_title = 'Room Booking';
$post_title_two = 'Room Booking Step 2';

if(!slug_exists($page_slug,'page'))
{
    // Create post object for room booking page
	$post = array(
        'post_title'    => wp_strip_all_tags($post_title),
        'post_content'  => $shortcode,
        'post_status'   => 'publish',
        'post_author'   => $author_id,
        'post_name'     => $page_slug,
        'post_type'     => 'page',
    );

    // Insert the post into the database
    $new_page_id = wp_insert_post( $post );
    update_post_meta( $new_page_id, '_wp_page_template', 'custom-room-details.php' );

}

if(!slug_exists($page_slug_two,'page')) 
{
    // Create post object for room booking page step two
    $postTwo = array(
        'post_title'    => wp_strip_all_tags($post_title_two),
        'post_content'  => $shortcode_two,
        'post_status'   => 'publish',
        'post_author'   => $author_id,
        'post_name'     => $page_slug_two,
        'post_type'     => 'page',
    );
    
    // Insert the post into the database
    $new_page_id_two = wp_insert_post( $postTwo );
}


/* Slug Checking function */
function slug_exists($post_name, $post_type) {
    global $wpdb;
    if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "' AND post_type = '" . $post_type . "'", 'ARRAY_A')) {
        return true;
    } else {
        return false;
    }
}