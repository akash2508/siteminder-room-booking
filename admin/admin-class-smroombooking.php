<?php
class siteminder_roombooking_admin{
	public function __construct(){

		add_action( 'admin_menu', array($this,'admin_menu'));
		add_action( 'admin_init', array($this,'register_room_booking_setting') );
		add_filter( 'plugin_action_links_siteminder-room-booking/siteminder-room-booking.php', array($this,'settings_link'));
		add_action( 'admin_enqueue_scripts',array($this,'smroombook_admin_enqueue_script'));		
		
	}

	public function smroombook_admin_enqueue_script(){
		wp_register_script('smroombook-admin-booking-script', SMROOM__PLUGIN_URL.'/admin/js/admin-script.js', array('jquery'), '1.0.1', true);
		wp_enqueue_script('smroombook-admin-booking-script');
	}

	public function admin_menu()
	{
		add_menu_page(
						__( 'Room Booking', 'smroombook' ),
						__( 'Room Booking', 'smroombook' ),
						'manage_options',
						'smroombook-settings',
						array($this,'view')
					);
	}

	public function view()
	{
	    include(SMROOM__PLUGIN_DIR.'/admin/admin-settings.php');
	}	

	public function settings_link( $links )
	{

		array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=smroombook-settings&tab=how_to_use' ) . '">' . ( 'How to use' ) . '</a>' );
		array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=smroombook-settings' ) . '">' . ( 'Settings' ) . '</a>' );
		
		return $links;
	}

	public function register_room_booking_setting()
	{ 
	  register_setting( 'rb_options_group', 'rb_display_view' ); //Field Register
	}
	
}


    