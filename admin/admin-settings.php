<div class="rb_options_main wrap">
	
	<h2>Room Booking Options</h2>
	<h2 class="nav-tab-wrapper">
	    <a href="?page=smroombook-settings" class="nav-tab">General Settings</a>
	    <a href="?page=smroombook-settings&tab=how_to_use" class="nav-tab">How to Use</a>
	</h2>

	<?php 
		if(!isset($_GET['tab'])) {
		?>
		<div class="wrap">
			<form method="post" action="options.php">
				<?php
					settings_fields('rb_options_group');
					$view_options = get_option('rb_display_view');

					//dynamic shortcode
					$view_sc = "style="."'".$view_options['type']."'";
					$new_code = str_replace(array( '[', ']' ), '', '[smroombook_booking]').' '.$view_sc;
					$shortcode_value = "[".$new_code."]";
				?>
				<table class="form-table" role="presentation">
	                <tbody>
		                	<tr>
		                		<th scope="row">Display View Options</th>
		                		<td align="left">
		                			<label><input type="radio" name="rb_display_view[type]" value="horizontal" <?php if($view_options['type'] == '' || $view_options['type']=='horizontal') { ?> checked="checked" <?php } ?> />Horizontal</label>
		                			<label><input type="radio" name="rb_display_view[type]" value="vertical" <?php checked('vertical', $view_options['type']); ?> />Vertical</label>
		                		</td>
		                	</tr>
			                <tr>
			                	<th scope="row">Shortcode</th>
			                	<td align="left">
			                		<label><input type="text" readonly name="view_shortcode" value="<?php echo $shortcode_value; ?>" class="regular-text code"></label>
			                	</td>
			                </tr>
			                <tr>
			                	<th scope="row">Hotel Name</th>
			                	<td align="left">
			                		<label><input type="text" name="rb_display_view[hotel_name]" value="<?php echo $view_options['hotel_name']; ?>" class="regular-text code"></label>
			                	</td>
			                </tr>
		            </tbody>
	            </table>
	            <h2>Pages Settings</h2>
			     <table class="form-table" role="presentation">
	                <tbody>
			                <tr>
			                    <th scope="row">Room Booking</th>
			                    <td>
			                        <input type="text" readonly name="rb_display_view[page_title_one]" value="<?php if(isset($view_options['page_title_one'])) { echo $view_options['page_title_one']; } ?>" class="regular-text code">
			                        <a href="/room-booking" targer="_blank">View Page</a>
			                    </td>
			                </tr>
			                <tr>
			                    <th scope="row">Room Booking Step 2</th>
			                    <td>
			                        <input type="text" readonly name="rb_display_view[page_title_two]" value="<?php if(isset($view_options['page_title_two'])) {echo $view_options['page_title_two'];} ?>" class="regular-text code">
			                        <a href="/room-booking-step-2" targer="_blank">View Page</a>
			                    </td>
			                </tr>
		            </tbody>
	            </table>     
	            <p> <?php submit_button(); ?></p>
			</form>
		<div>	
	<?php } else { ?>

		<div class="sm-how-to-use wrap">
				<h1>Welcome To Geekunique Room Booking Plugin</h1>
				<p>Room booking by Geekunique provides an easy way to book rooms. You will be provided shortcode to display room booking form in page or widget.</p>

				<h2>How to display shortcode</h2>
				<p>We have provided an option in shortcode to pass attribute to display booking form in website. Add below shortcode in the page to display room booking form</p>
				<p><strong>Display horizontal:</strong> <code>[smroombook_booking style='horizontal']</code></p>
				<p><strong>Display Vertical:</strong> <code>[smroombook_booking style='vertical']</code></p>
		</div>
	<?php } ?>
</div>