jQuery(document).ready(function(){
    jQuery('.rb_options_main input[type="radio"]').change(function(){
        var value = jQuery('.rb_options_main input[type="radio"]:checked').val();
        var shortcode = jQuery('.rb_options_main input[name="view_shortcode"]').val();

        /* Get Value Of view */
        var start_pos = shortcode.indexOf("'") + 1;
    	var end_pos = shortcode.indexOf("'",start_pos);
    	var text_to_get = shortcode.substring(start_pos,end_pos);
        
        /* Replace value which checked */
        var result = shortcode.replace(text_to_get, value);
        jQuery('.rb_options_main input[name="view_shortcode"]').val(result);
    	
    });
});