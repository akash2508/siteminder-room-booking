<?php
class siteminder_roombooking{

	public function __construct(){

		add_action( 'wp_enqueue_scripts',array($this,'smroombook_front_enqueue'));
		add_shortcode( 'smroombook_booking', array($this,'smroombook_booking_shortcode'));
		add_action( 'template_include', array($this,'uploadr_redirect') );
		add_shortcode( 'smroombook_dynamic_iframe', array($this,'smroombook_dynamic_iframe_shortcode') );
	}

	public function smroombook_front_enqueue() {
		$params = array(
	        'ajaxurl' => admin_url('admin-ajax.php'), 
	        'nonce' => wp_create_nonce( "process_reservation_nonce" ),
	    );

	    //CSS
		wp_register_style('t-datepicker-css', SMROOM__ASSETS.'css/t-datepicker.min.css');
		wp_register_style('t-datepicker-theme-css', SMROOM__ASSETS.'css/themes/t-datepicker-bluegrey.css');
		wp_register_style('smroombook-booking-style', SMROOM__ASSETS.'css/smroombook-booking-style.css');
		wp_register_style('smroombook-owl-carousel-style', SMROOM__ASSETS.'css/owl.carousel.min.css');

		//JS
		wp_register_script('t-datepicker-js', SMROOM__ASSETS.'js/t-datepicker.min.js', array('jquery'), '1.0.1', true);
		wp_register_script('smroombook-booking-script', SMROOM__ASSETS.'js/smroombook-booking-script.js', array('jquery'), '1.0.0', true);
		wp_localize_script('smroombook-booking-script', 'frontend_ajax_object', $params );
		wp_register_script('smroombook-owl-carousel-script', SMROOM__ASSETS.'js/owl.carousel.min.js', array('jquery'), '1.0.0', true);

		wp_enqueue_style('t-datepicker-css');
		wp_enqueue_style('t-datepicker-theme-css');
		wp_enqueue_style('smroombook-booking-style');
		wp_enqueue_style('smroombook-owl-carousel-style');

		wp_enqueue_script('t-datepicker-js');
		wp_enqueue_script('smroombook-booking-script');
		wp_enqueue_script('smroombook-owl-carousel-script');
	}


	public function smroombook_booking_shortcode($atts) {
		global $wp, $wpdb;

        $style = $atts['style'];
        if($style!='vertical') {
            $showFields = 'style="display: none;"';
        } else {
            $showFields = '';
        }
        
		$room_options = '';
        $room_selected = '';
        $adult_options = '';
        $adult_selected = '';
        $children_options = '';
        $children_selected = '';

    	for ($i=1; $i <= 4; $i++) {

    	    if(isset($_GET['smroombook_rooms']) && $_GET['smroombook_rooms'] == $i){
        	    $room_selected = 'selected=selected';
        	}
            else{
                $room_selected = '';
            }
            if($i == 1){
                $room_options .= '<option '.$room_selected.' value="'.$i.'">'.$i.' Room</option>';    
            }else{
                $room_options .= '<option '.$room_selected.' value="'.$i.'">'.$i.' Rooms</option>';
            }
        }
        for ($i=1; $i <= 10; $i++) {

            if(isset($_GET['smroombook_adults']) && $_GET['smroombook_adults'] == $i){
                $adult_selected = 'selected=selected';
            }
            else{
                $adult_selected = '';
            }
            
            $adult_options .= '<option '.$adult_selected.' value="'.$i.'">'.$i.'</option>';
        }
        for ($i=1; $i <= 10; $i++) {

            if(isset($_GET['smroombook_children']) && $_GET['smroombook_children'] == $i){
                $children_selected = 'selected=selected';
            }
            else{
                $children_selected = '';
            }
            $children_options .= '<option '.$children_selected.' value="'.$i.'">'.$i.'</option>';
        }

		$html = '<form action="/room-booking" class="book-room-form-coll site-booking-form d-flex flex-wrap form-'.$atts['style'].'" method="get">';
				if(is_singular('room')){
					$html .= '<input type="hidden" value="'.get_the_title().'" name="room_type">';
				}
			$html .= '<div class="site_form_body">
						<ul class="site_form_fields d-flex flex-wrap">
							<li class="site_form_field book-filed-one site-date-field">
								<div class="t-datepicker smroombook-checkinout">
									<label class="site_form_label" for="t-check-in">Check In
						  				<div class="t-check-in"></div>
						  			</label>
						  			<label class="site_form_label" for="t-check-out">Check Out
						  				<div class="t-check-out"></div>
						  			</label>
								</div>
							</li>
							<li class="site_form_field book-filed-two" '.$showFields.'>
								<label class="site_form_label" for="smroombook_rooms">Rooms</label>
								<div class="site_form_input_container">
									<select id="smroombook_rooms" name="smroombook_rooms">'.$room_options.'</select>
								</div>	
							</li>
							<li class="site_form_field book-filed-three" '.$showFields.'>
								<label class="site_form_label" for="smroombook_adults">Adult</label>
								<div class="site_form_input_container">
									<select id="smroombook_adults" name="smroombook_adults">'.$adult_options.'</select>
								</div>	
							</li>
							<li class="site_form_field book-filed-four" '.$showFields.'>
								<label class="site_form_label" for="smroombook_children">Children</label>
								<div class="site_form_input_container">
									<select id="smroombook_children" name="smroombook_children"><option value="0">0</option>'.$children_options.'</select>
								</div>
							</li>
							<li class="site_form_footer-btn" '.$showFields.'>
								<div class="site_form_footer">
									<input type="submit" value="Book a Room" class="site_form_button" id="smroombook_submit"/>
								</div>
							</li>
						</ul>';
						if($style!='vertical') {
						$html .= '<div class="slide-down-form-2" id="slide-down-form-2" style="cursor: pointer;">
									<i class="fas fa-chevron-down"></i>
                         		</div>';
						}
					$html .= '</div>
				</form>';

		return $html;
	}

	public	function uploadr_redirect( $template ) {

        if ( is_page( 'room-booking' ) ) {
            $template = SMROOM__PLUGIN_DIR . '/templates/room-details.php';
        }
        
        if ( is_page( 'room-booking-step-2' ) ) {
            $template = SMROOM__PLUGIN_DIR . '/templates/room-details-step-two.php';
        }
        return $template;
    }

    public function smroombook_dynamic_iframe_shortcode(){
	    $html = '';
	    $uri = urldecode('https://app.thebookingbutton.com/frog-mill-shipton/properties/thefrogmilhoteldirect/book?check_in_date='.$_GET['check_in_date'].'&check_out_date='.$_GET['check_out_date'].'&number_adults=2&rooms='.$_GET['rooms'].'');
	    $html .= '<iframe id="iframe" src="'.$uri.'" width="100%" height="1780px"></iframe>';
	    return $html;
	}
}
