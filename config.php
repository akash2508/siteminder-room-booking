<?php
if(!defined('SMROOM_VERSION'))
{
	define( 'SMROOM_VERSION', '1.0.0');
}

if(!defined('SMROOM__MINIMUM_WP_VERSION'))
{
	define( 'SMROOM__MINIMUM_WP_VERSION', '4.0');
}

if(!defined('SMROOM_BASE_DIR'))
{
	define( 'SMROOM_BASE_DIR', plugin_basename(__FILE__));
}

if(!defined('SMROOM__PLUGIN_DIR'))
{
	define( 'SMROOM__PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ));
}

if(!defined('SMROOM__PLUGIN_URL'))
{
	define( 'SMROOM__PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ));
}

if(!defined('SMROOM__API_URL'))
{
	define( 'SMROOM__API_URL', 'https://app.thebookingbutton.com/api/v2');
}

if(!defined('SMROOM__ASSETS'))
{
	define( 'SMROOM__ASSETS', SMROOM__PLUGIN_URL . '/assets/');
}

if ( !defined( 'RB_SHORTCODE' ) ) {
    define( 'RB_SHORTCODE', '[smroombook_booking style="horizontal"]' );
}

if ( !defined( 'RB_SHORTCODE_IFRAME' ) ) {
    define( 'RB_SHORTCODE_IFRAME', '[smroombook_dynamic_iframe]' );
}