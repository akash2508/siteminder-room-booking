/*
* Siteminder Room Booking Script
* Version: v1.0.0
*/

jQuery(document).ready(function($) {
	
	var params = new window.URLSearchParams(window.location.search);
    var startdate = '';
    var enddate = '';
    
    /**Set current date if checkin and checkout date is not querystring***/
    var today = new Date().toJSON().slice(0,10).replace(/-/g,'-');
  	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);
	
	if(params.get('t-start'))
	{
		startdate = params.get('t-start');
	}
	else
	{
		startdate = today;	
	}
    
    if(params.get('t-end'))
	{
		enddate = params.get('t-end');
	}
	else
	{
		enddate = tomorrow;
	}

	jQuery('.smroombook-checkinout').tDatePicker({
		iconDate :'<i class="fas fa-calendar-alt"></i>',
		dateCheckIn : startdate,
	    dateCheckOut : enddate,
	    numCalendar : 1
	});

	/**Room images slider***/
	jQuery('.room-info__gallery').owlCarousel({
        loop:false,
        nav:true,
        dot:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

});