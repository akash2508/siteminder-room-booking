<?php

/**

 * Plugin Name: Geekyunique Room Booking
 * Plugin URI: https://www.geekunique.co.uk/
 * Description: Widget to book rooms using Siteminder APIs
 * Version: 1.0.0
 * Text Domain: smroombook
 * Domain Path: /languages
 * License: GNU General Public License v2.0 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Plugin Variable: smroombook
 */

if( !defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly

require_once 'config.php';
require_once SMROOM__PLUGIN_DIR . '/includes/class-smroombooking.php';
require_once SMROOM__PLUGIN_DIR . '/admin/admin-class-smroombooking.php';

function smroombook_plugin_init() {
  $langOK = load_plugin_textdomain( 'smroombook', false, dirname( plugin_basename(__FILE__) ) .'\languages' );
}
add_action('plugins_loaded', 'smroombook_plugin_init');


//this method initialize the class
function sm_roombook_plugin_init()
{
	if(is_admin())
	{
		new siteminder_roombooking_admin();	
	}
	else{
		new siteminder_roombooking();
	}
}

add_action('init', 'sm_roombook_plugin_init');



register_activation_hook(__FILE__,'smroombook_plugin_activate'); //activate hook
//register_deactivation_hook(__FILE__, 'plugin_deactivate'); //deactivate hook

function smroombook_plugin_activate() {
	require_once SMROOM__PLUGIN_DIR . '/admin/admin-create-page.php';
}