<?php
/***
Template Name: Room Details
***/
?>

<div class="dark-header roombooking-step-one">

<?php get_header();

    if(!empty($_GET['t-start']) && isset($_GET['t-start'])){
        $checkIn = $_GET['t-start'];    
    }else{
        $checkIn = date("Y-m-d");
    }
    
    if(!empty($_GET['t-end']) && isset($_GET['t-end'])){
        $checkOut = $_GET['t-end']; 
    }else{
        $checkOut = date('Y-m-d', strtotime(' +1 day'));
    }
    
    $newcheckIn = date("d-m-Y", strtotime($checkIn));
    $newcheckOut = date("d-m-Y", strtotime($checkOut));
    
    $start_date = strtotime($checkIn); 
    $end_date = strtotime($checkOut); 
    

    $day_difference = ($end_date - $start_date)/60/60/24;
    
    ?>
<div class="search-room-page">
    <section class="select-your-room-in section-padding bg-EBEAE7 pb-0">
        <div class="container-md">
             <div class="row">
                <div class="col-md-12">
                  <div class="text-center room-select-full ">
                    <h2 class="section__header">Select your room</h2>
                    <p><span class="room-info__price-value">Price for <?php echo $day_difference; ?> night</span></p>
                  </div> 
                </div>
             </div>
        </div>
    </section>
  
    <?php  

        $room_options = '';
        $room_selected = '';
        $adult_options = '';
        $adult_selected = '';
        $children_options = '';
        $children_selected = '';

        for ($i=1; $i <= 4; $i++) {

            if(isset($_GET['smroombook_rooms']) && $_GET['smroombook_rooms'] == $i){
                $room_selected = 'selected=selected';
            }
            else{
                $room_selected = '';
            }
            if($i == 1){
                $room_options .= '<option '.$room_selected.' value="'.$i.'">'.$i.' Room</option>';    
            }else{
                $room_options .= '<option '.$room_selected.' value="'.$i.'">'.$i.' Rooms</option>';
            }
        }
        for ($i=1; $i <= 10; $i++) {

            if(isset($_GET['smroombook_adults']) && $_GET['smroombook_adults'] == $i){
                $adult_selected = 'selected=selected';
            }
            else{
                $adult_selected = '';
            }
            
            $adult_options .= '<option '.$adult_selected.' value="'.$i.'">'.$i.'</option>';
        }
        for ($i=1; $i <= 10; $i++) {

            if(isset($_GET['smroombook_children']) && $_GET['smroombook_children'] == $i){
                $children_selected = 'selected=selected';
            }
            else{
                $children_selected = '';
            }
            $children_options .= '<option '.$children_selected.' value="'.$i.'">'.$i.'</option>';
        }
    ?>
    




    <section class="search-room-all section-padding pt-0 bg-EBEAE7">
        <div class="container-md">
             <div class="row">  
                <div class="col-md-12">  
                <form action="/room-booking" class="book-room-form-coll site-booking-form d-flex flex-wrap form-horizontal " method="get">
                    <div class="site_form_body">
                        <ul class="site_form_fields d-flex flex-wrap">
                            <li class="site_form_field book-filed-one site-date-field">
                                <div class="t-datepicker smroombook-checkinout">
                                    <label class="site_form_label" for="t-check-in">Check In
                                        <div class="t-check-in smroombook_checkin"></div>
                                    </label>
                                    <label class="site_form_label" for="t-check-out">Check Out
                                        <div class="t-check-out smroombook_checkout"></div>
                                    </label>
                                </div>
                            </li>
                            <li class="site_form_field book-filed-two" style="display: none;">
                                <label class="site_form_label" for="smroombook_rooms">Rooms</label>
                                <div class="site_form_input_container">
                                    <select id="smroombook_rooms" name="smroombook_rooms"><?php echo $room_options; ?></select>
                                </div>  
                            </li>
                            <li class="site_form_field book-filed-three" style="display: none;">
                                <label class="site_form_label" for="smroombook_adults">Adults</label>
                                <div class="site_form_input_container">
                                    <select id="smroombook_adults" name="smroombook_adults"><?php echo $adult_options; ?></select>
                                </div>  
                            </li>
                            <li class="site_form_field book-filed-four" style="display: none;">
                                <label class="site_form_label" for="smroombook_children">Children</label>
                                <div class="site_form_input_container">
                                    <select id="smroombook_children" name="smroombook_children"><option value="0">0</option><?php echo $children_options; ?></select>
                                </div>
                            </li>
                            <li class="site_form_footer-btn" style="display: none;">
                                <div class="site_form_footer">
                                    <input type="submit" value="Book a Room" class="site_form_button" id="smroombook_submit"/>
                                </div>
                            </li>
                        </ul>  
                        <div class="slide-down-form-2" id="slide-down-form-2" style="cursor: pointer;">
                            <i class="fas fa-chevron-down"></i>
                        </div>  
                   </div>
                </form> 
         </div>
         </div>
        </div>
    </section>  


        
    <div class="room-page__section">
        <div class="section-padding pt-0 bg-EBEAE7">
            <div class="container-padding">
            <?php
                
                $data = array(
                  'room_data'  => 'https://app.thebookingbutton.com/api/v1/properties/thefrogmilhoteldirect/rates.json?start_date='.$checkIn.'&end_date='.$checkOut,
                  'room_types'         => SMROOM__API_URL.'/reloaded/properties/thefrogmilhoteldirect/room_types',
                  'rooms_availability' => SMROOM__API_URL.'/reloaded/properties/thefrogmilhoteldirect/rooms_availability?end_date='.$checkOut.'&start_date='.$checkIn,
                  'room_type_dates' => SMROOM__API_URL.'/reloaded/properties/thefrogmilhoteldirect/room_type_dates?check_in_date='.$checkIn.'&check_out_date='.$checkOut,
                  'room_rates' => SMROOM__API_URL.'/reloaded/properties/thefrogmilhoteldirect/room_rates?check_in_date='.$newcheckIn.'&check_out_date='.$newcheckOut,
                  'room_rate_dates' => SMROOM__API_URL.'/reloaded/properties/thefrogmilhoteldirect/room_rate_dates?check_in_date='.$checkIn.'&check_out_date='.$checkOut
                );   
                
                $roombook_data = smroombook_multiRequest($data);
                
                $unavailable_room_id = array();
                $available_room_id = array();
                $room_type_id = array();
                $available_room_type_id = array();
                $unavailable_room_type_id = array();
                $global_room_data = array();
                
                foreach($roombook_data['room_type_dates'] as $id => $k){
                    if($k->available == 0){
                        $unavailable_room_id[] = $k->room_type_id;
                    }
                }
                
                foreach($roombook_data['room_type_dates'] as $k => $v){
                    if(!in_array($v->room_type_id, $unavailable_room_id)){
                        $available_room_id[] = $v->room_type_id;
                    }
                }
                $available_room_id = array_unique($available_room_id);
                
                /***check room types id available from room id***/
                foreach($available_room_id as $room_id){
                    
                    foreach($roombook_data['room_types'] as $room_type){
                        if($room_type->id == $room_id){
                            $global_room_data[] = array('room_id' => $room_id, 'room_name' => $room_type->name, 'room_description' => $room_type->description, 'room_photos' => $room_type->photos, 'room_top_five_features' => $room_type->top_five_features, 
                            'room_features' => $room_type->features);
                        }
                    }
                    foreach($roombook_data['room_rates'] as $room_rates){
                        if($room_rates->room_type_id == $room_id){
                            $room_type_id[] = array('room_id' => $room_id, 'room_type_id' => $room_rates->id, 'room_type_description' => $room_rates->description, 'room_type_name' => $room_rates->name);
                        }
                    } 
                }
                
                
                foreach($room_type_id as $k_id => $r_id){
                    foreach($roombook_data['room_rate_dates'] as $room_rate_date){
                        $room_temp = $r_id['room_id'];
                        $room_type_temp = $r_id['room_type_id'];
                        if($room_type_temp == $room_rate_date->room_rate_id){
                            $available_room_type_id[] = array( 'room_id' => $room_temp, 'room_type_id' => $room_type_temp, 'price' => $room_rate_date->rate, 'room_type_name' => $r_id['room_type_name'], 'room_type_description' => $r_id['room_type_description'], 'min_stay' => $room_rate_date->min_stay, 'stop_sale' => $room_rate_date->stop_sell );    
                        }
                    } 
                }
                $available_room_type_id = array_unique($available_room_type_id, SORT_REGULAR);

                $result = array_reduce($available_room_type_id, function ($result, $available_room_type_id) {
                    $productId = $available_room_type_id['room_type_id'];
                    if(isset($result[$productId])) {
                        $result[$productId]['price'] += $available_room_type_id['price'];
                        return $result;
                    }
                    $result[$productId] = $available_room_type_id;
                    return $result;
                }, []);

                $price_array = array_column($result, 'price');
                array_multisort($price_array, SORT_ASC, $result);

                if(!empty($global_room_data)){

                    foreach($global_room_data as $k => $v){
                        ?>
                        <div class="room-info">
                        <div class="row">
                            <input type="hidden" class="smbook-checkin" name="checkIn-date" value="<?php echo $checkIn; ?>">
                            <input type="hidden" class="smbook-checkout" name="checkOut-date" value="<?php echo $checkOut; ?>">
                            <div class="col-md-12 col-lg-4 room-gallery-main">
                                <div class="room-info__gallery owl-carousel owl-theme site-slider-df-arrow ">
                                    <?php 
                                        foreach($v['room_photos'] as $photo){
                                            ?><div class="item"><div class="room-infogallery-bg" style="background-image:url(<?php echo $photo->large_url; ?>); height:300px;"></div></div><?php
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-4 gutter-30 room-info-main">
                                <div class="room-info__info">
                                    <div class="room-info__info-header">
                                        <h4 class="room-info__title ">
                                            <?php echo $v['room_name']; ?>
                                        </h4>
                                        <a class="more-info btn btn-info btn-lg antwerp-light-italic " data-toggle="modal" data-target="#moreinfo-<?php echo $v['room_id']; ?>" href="javascript:void(0);"><span>i</span></a>


                                       
                                        <div id="moreinfo-<?php echo $v['room_id']; ?>" class="modal room-sec-pop fade" role="dialog" aria-labelledby="modal-title" aria-describedby="modal-content">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>    
                                                    <div id="modal-body">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="modal-descriptions bottom-spacer">
                                                                        <h4 id="modal-title" class="modal-title" tabindex="0"><?php echo $v['room_name']; ?></h4>
                                                                        <p class="bottom-spacer line-break"><?php echo $v['room_description']; ?></p> 
                                                                        <ul class="room__features">
                                                                            <?php
                                                                                foreach($v['room_top_five_features'] as $top_feature){
                                                                                    ?><li><span><?php echo $top_feature->name; ?></span></li><?php
                                                                                }
                                                                            ?>
                                                                        </ul>
                                                                    </div>    
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <?php 
                                                                        foreach($v['room_photos'] as $photo){
                                                                            ?><div class="room-full-img"><div class="room-full-img-bg" style="background-image:url(<?php echo $photo->standard_url; ?>);"></div></div><?php
                                                                        }
                                                                    ?> 
                                                                </div>
                                                            </div>    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="room-info__info-body">
                                        <p class="room-info__info-body-text ">
                                            <?php echo $v['room_description']; ?>
                                        </p>
                                        <ul class="room__features ">
                                            <?php
                                                foreach($v['room_top_five_features'] as $top_feature){
                                                    ?><li><span><?php echo $top_feature->name; ?></span></li><?php
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-4 room-price-main">
                                <div class="room-info__prices">
                                    <div class="room-info__price room-info__price--header">
                                        <span class="room-info__price-value">Price for <?php echo $day_difference; ?> night</span>
                                    </div>
                                    <?php 
                                        foreach($result as $a1 => $b1){
                                            if($b1['room_id'] == $v['room_id'] && $b1['stop_sale'] != 1){
                                            ?>
                                                <div class="room-info__price">
                                                    <div class="room-info__price-hold ">
                                                        <span class="room-info__price-title"><?php echo $b1['room_type_name']; ?></span>
                                                        <span class="room-info__price-value"><?php echo "&pound;".$b1['price']; ?></span>
                                                    </div>
                                                    
                                                    <a class="button button--book room-info__price-book " room_name="<?php echo $v['room_name']; ?>" room_id="<?php echo $b1['room_type_id']; ?>" room_price="<?php echo ($day_difference * $b1['price']); ?>" target="_blank" href="/room-booking-step-2/?check_in_date=<?php echo trim($checkIn); ?>&check_out_date=<?php echo trim($checkOut); ?>&number_adults=<?php echo $_GET['smroombook_rooms']; ?>&rooms=<?php echo trim($b1['room_type_id']); ?>">Select</a>
                                                    
                                                    <a class="more-info antwerp-light-italic room-info__price-info-icon btn btn-info btn-lg " data-toggle="modal" data-target="#roominfo-<?php echo $b1['room_type_id']; ?>" data-mfp-src="#roominfo-<?php echo $b1['room_type_id']; ?>" href="javascript:void(0);"><span>i</span></a>


                                                    <div id="roominfo-<?php echo $b1['room_type_id']; ?>" class="modal room-model-pop" role="dialog" aria-labelledby="modal-title" aria-describedby="modal-content">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                </div>    
                                                                <div id="modal-body">
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="modal-descriptions bottom-spacer">
                                                                                    <h4 id="modal-title" class="modal-title" tabindex="0"><?php echo $b1['room_type_name']; ?></h4>
                                                                                    <p class="bottom-spacer line-break"><?php echo $b1['room_type_description']; ?></p> 
                                                                                </div>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            <?php
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                }else{
                    ?><div class="alert-msg text-center">Sorry, there is no availability for your selected dates. Please choose different dates. Alternatively you can call us on +441242 386364.</div><?php
                }
                
        ?>
    </div>
        </div>
    </div>
</div>
<?php
        
    function smroombook_multiRequest($data, $options = array()) {
    
        $curly = array();
        $result = array();
        $mh = curl_multi_init();
        
        foreach ($data as $id => $d) {
        
            $curly[$id] = curl_init();
            
            $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
            curl_setopt($curly[$id], CURLOPT_URL,            $url);
            curl_setopt($curly[$id], CURLOPT_HEADER,         0);
            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
            
            
            if (is_array($d)) {
                if (!empty($d['post'])) {
                    curl_setopt($curly[$id], CURLOPT_POST,       1);
                    curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
                }
            }
        
            if (!empty($options)) {
                curl_setopt_array($curly[$id], $options);
            }
        
            curl_multi_add_handle($mh, $curly[$id]);
        }
        
        $running = null;
        
        do {
            curl_multi_exec($mh, $running);
        } while($running > 0);
        
        foreach($curly as $id => $c) {
            $result[$id] = json_decode(curl_multi_getcontent($c));
            curl_multi_remove_handle($mh, $c);
        }
        
        curl_multi_close($mh);
        
        return $result;
    }
        

get_footer(); ?>
</div>