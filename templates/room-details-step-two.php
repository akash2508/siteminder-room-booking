<?php
/***
Template Name: Room Details Step Two
***/
?>
<div class="roombooking-step-two dark-header">

<?php
	get_header();
	global $post;

	$postContent = $post->post_content;
	echo do_shortcode($postContent);

?>
</div>